//Setting the accordion as specified on jQueryUI
(function () {
$( '#accordion' ).accordion({
	animate: 200,
	collapsible: true,
	active: false,
	icons: true,
	heightStyle: 'content'
});
$('.ui-accordion-header-icon').html('+');	//Adding the "+" sign to the span at the beginning of each question
$('.ui-icon:even').addClass('yellow_box');	//Making sure the even spans get the background color Yellow
$('.ui-icon:odd').addClass('blue_box');		//Making sure the odd spans get the background color Blue
//Each time a question looses focus return to the "+" sign

var acordion_header = $('.ui-accordion-header');

acordion_header.on('blur', function(event){
	var target = $(event.target);
	target.children('span').html('+');
});

//Each time a question gets clicked the simbol in his span element will change from "+" to "-" and viceversa
acordion_header.on('click', function(event){
	var target = $(event.target);
	if (target.children('span').html() == '+')
		target.children('span').html('-');
	else
		target.children('span').html('+');
});

})();