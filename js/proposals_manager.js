(function() {
	$('.proposal_item').on('click', function(event){
		var element = $(this);
		if (element.children('div.proposal_top').hasClass('closed')) {
			$('.proposal_bottom').addClass('hidden');
			$('.proposal_top').addClass('closed');
			$('p.separator').html('+');
			element.children('div.proposal_top').removeClass('closed');
			element.children('div.proposal_bottom').removeClass('hidden');
			element.children('p.separator').html('-');
		} else {
			element.children('div.proposal_top').addClass('closed');
			element.children('div.proposal_bottom').addClass('hidden');
			element.children('p.separator').html('+');
		}
	});
})();