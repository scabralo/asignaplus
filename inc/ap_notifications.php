<?php
/**
 * AsignaPlus - Notifications Management System
 * 
 * This system will manage and show the different messages that AsignaPlus needs to show to the users.
 * This looks for the 'msg' variable on the $_GET global variable and shows a message according to the
 * content of that variable.
 */

//Check if $_GET variable is set and if 'msg' variable exists
if(isset($_GET['msg'])) {
//Check if the content ofthe variable is as expected (integer)
$error_msg = absint($_GET['msg']);

$notification_html = '';
$notification_html .= '<div id="notifications_area">';
//Here we'll add all the notifications to the user
if ($error_msg > 0) {
	$notification_html .= '<p id="message" ';
	switch ($error_msg) {

		case 1: //Assignment created succesfuly
			$notification_html .= 'class="success">';
			$notification_html .= __(esc_html('La asignación fue creada exitosamente!'), 'asignaplus');
			break;

		case 2: //An error has occured while creating assignation
			$notification_html .= 'class="failure">';
			$notification_html .= __(esc_html('Error, su asignación no pudo ser creada.'), 'asignaplus');
			break;

		case 3: //The Assignment has been deleted succesfuly
			$notification_html .= 'class="information">';
			$notification_html .= __(esc_html('La asignación fue eliminada exitosamente.'), 'asignaplus');
			break;

		case 4: //The Assignment has been edited succesfuly
			$notification_html .= 'class="information">';
			$notification_html .= __(esc_html('La asignación fue editada exitosamente.'), 'asignaplus');
			break;

		case 5: //The Assignment doesn't exists
			$notification_html .= 'class="information">';
			$notification_html .= __(esc_html('La asignación que busca no está disponible.'), 'asignaplus');
			break;

		case 6: //Error creating proposal
			$notification_html .= 'class="failure">';
			$notification_html .= __(esc_html('Error, su propuesta no pudo ser creada.'), 'asignaplus');
			break;

		case 7: //Success creating proposal
			$notification_html .= 'class="success">';
			$notification_html .= __(esc_html('Su propuesta ha sido enviada, buena suerte!'), 'asignaplus');
			break;

		case 8: //Information: You can't apply to solve your own assignment
			$notification_html .= 'class="information">';
			$notification_html .= __(esc_html('No puede aplicar para una asignación que usted haya creado.'), 'asignaplus');
			break;

		case 9: //Information: You can't apply more than once to a single assignment
			$notification_html .= 'class="information">';
			$notification_html .= __(esc_html('No puede aplicar más de una vez por asignación.'), 'asignaplus');
			break;

		case 10: //Information: You can't apply to solve your own assignment
			$notification_html .= 'class="information">';
			$notification_html .= __(esc_html('La propuesta que busca no está disponible.'), 'asignaplus');
			break;

		case 11: //Success: The Assignment has been eliminated succesfully
			$notification_html .= 'class="success">';
			$notification_html .= __(esc_html('La asignación fue eliminada exitosamente.'), 'asignaplus');
			break;

		case 12: //Error: The Assignment couldn't be eliminated 
			$notification_html .= 'class="failure">';
			$notification_html .= __(esc_html('La asignación no pudo ser eliminada.'), 'asignaplus');
			break;

		case 13: //Information: The Assignment has been updated succesfully
			$notification_html .= 'class="success">';
			$notification_html .= __(esc_html('La asignación fue actualizada exitosamente.'), 'asignaplus');
			break;

		case 14: //Information: The Assignment has been updated succesfully
			// I don't think we need to show a message when we post a message, 
			// I think we better show it when there's an error, I'm saving the number just in case
			// we want to use it later
			break;

		case 15: //Error: Your message couldn't be sent, please try again later
			$notification_html .= 'class="failure">';
			$notification_html .= __(esc_html('Su mensaje no pudo ser enviado, por favor vuelva a intentarlo.'), 'asignaplus');
			break;

		case 16: //Success: The proposal was accepted successfully
			$notification_html .= 'class="success">';
			$notification_html .= __(esc_html('La propuesta fue aceptada exitosamente.'), 'asignaplus');
			break;

		case 17: //Error: This proposal couldn't be accepted
			$notification_html .= 'class="failure">';
			$notification_html .= __(esc_html('La propuesta no pudo ser aceptada, por favor vuelva a intentarlo.'), 'asignaplus');
			break;

		case 18: 
			$notification_html .= 'class="failure">';
			$notification_html .= __(esc_html('Los fondos no puedieron ser liberados, por favor vuelva a intentarlo más tarde.'), 'asignaplus');
			break;

		//default:
		//break;
	}
	$notification_html .= '</p>';
}
$notification_html .= '</div><!-- #notifications_area -->';
echo $notification_html;
}
?>