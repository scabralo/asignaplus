<?php
/**
 * AsignaPlus - Page Title Bar
 * 
 * This file will show the page_title_bar. In this part of the site we'll show the Log out / Log in / Create account
 */
echo '<div id="title_bar_links">';
if (is_user_not_logged_in()) {
	echo '<a href="/wp-login.php">log in</a> / <a href="/wp-login.php?action=register">crear cuenta</a>';
} else {
	echo '<a href="' . wp_logout_url() . '">salir</a>';
}
echo '</div>';
?>