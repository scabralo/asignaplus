<?php
class Payment
{
	const TABLE_TAREAS = "ap_tareas";
  const TABLE_PROPOSALS = "ap_propuestas";
  const TABLE_PAYMENTS = "ao_payments";

  protected $id = '';
  protected $assign_id = '';
  protected $assign_title = '';
  protected $date_created = '';
  protected $date_deposit = '';

  public function __construct($new_id = 0) {
  	if ($new_id != 0) {
  		global $wpdb;

			$query = "SELECT * FROM ". self::TABLE_PAYMEnTS ." WHERE id_payment = $new_id";
	    $payments_array = $wpdb->get_results($query);

	    if (count($payments_array) > 0) { //We check if the result from the query is empty

	      $payments_info = $payments_array[0];
	      $this->id = $payments_info->id_payment;
	      $this->$assign_id = $payments_info->assign_id;
	      $this->$assign_title = $payments_info->assign_title;
	      $this->$date_created = $payments_info->date_created;
	      $this->$date_deposit = $payments_info->date_deposit;
	    }
  	}
  }

  /**
   * Gets the value of id.
   *
   * @return mixed
   */
  public function getId()
  {
      return $this->id;
  }

  /**
   * Sets the value of id.
   *
   * @param mixed $id the id
   *
   * @return self
   */
  protected function setId($id)
  {
      $this->id = $id;

      return $this;
  }

  /**
   * Gets the value of assign_id.
   *
   * @return mixed
   */
  public function getAssignId()
  {
      return $this->assign_id;
  }

  /**
   * Sets the value of assign_id.
   *
   * @param mixed $assign_id the assign id
   *
   * @return self
   */
  protected function setAssignId($assign_id)
  {
      $this->assign_id = $assign_id;

      return $this;
  }

  /**
   * Gets the value of assign_title.
   *
   * @return mixed
   */
  public function getAssignTitle()
  {
      return $this->assign_title;
  }

  /**
   * Sets the value of assign_title.
   *
   * @param mixed $assign_title the assign title
   *
   * @return self
   */
  protected function setAssignTitle($assign_title)
  {
      $this->assign_title = $assign_title;

      return $this;
  }

  /**
   * Gets the value of date_created.
   *
   * @return mixed
   */
  public function getDateCreated()
  {
      return $this->date_created;
  }

  /**
   * Sets the value of date_created.
   *
   * @param mixed $date_created the date created
   *
   * @return self
   */
  protected function setDateCreated($date_created)
  {
      $this->date_created = $date_created;

      return $this;
  }

  /**
   * Gets the value of date_deposit.
   *
   * @return mixed
   */
  public function getDateDeposit()
  {
      return $this->date_deposit;
  }

  /**
   * Sets the value of date_deposit.
   *
   * @param mixed $date_deposit the date deposit
   *
   * @return self
   */
  protected function setDateDeposit($date_deposit)
  {
      $this->date_deposit = $date_deposit;

      return $this;
  }
}