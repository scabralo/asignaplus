<?php
/* 
 * This Class manages the CRUD for the proposals and proposal information
 * 
 * Table Name: ap_propuestas
 * Table Columns: 
 */
class ProposalsManager
{
	const TABLE_TAREAS = "ap_tareas";
    const TABLE_PROPOSALS = "ap_propuestas";
    const TABLE_USER_INFO = "ap_user_info";

    public function __construct() {
        //Code necessary to initialize the class
    }

	/*
     * This function returns an array with all the proposals created by an user and with the specified status
     * 
     */
    public function allProposalsByUser ($user_id, $proposal_status) {

        if ($user_id) {
            global $wpdb;
            $query = "SELECT "
            . self::TABLE_PROPOSALS .".date_created, "
            . self::TABLE_PROPOSALS .".id_assign, "
            . self::TABLE_PROPOSALS .".user_create AS prop_user_create, "
            . self::TABLE_TAREAS .".title, "
            . self::TABLE_TAREAS .".user_create_id, "
            . self::TABLE_TAREAS .".user_create FROM "
            . self::TABLE_PROPOSALS ." JOIN "
            . self::TABLE_TAREAS ." ON "
            . self::TABLE_PROPOSALS .".id_assign="
            . self::TABLE_TAREAS .".id_assign WHERE "
            . self::TABLE_PROPOSALS .".user_create_id = $user_id AND "
            . self::TABLE_PROPOSALS .".proposal_status = $proposal_status ORDER BY "
            . self::TABLE_PROPOSALS .".date_created ASC";

            $proposals_info = $wpdb->get_results($query);
            
            if (count($proposals_info) > 0) { //We check if the result from the query is empty
                return $proposals_info;
            }
        }

        return 0;
    }

    /*
     * This function returns an array with all the proposals created by an user and with the specified status
     * 
     */
    public function allProposalsByAssignment ($assign_id) {

        if ($assign_id) {
            global $wpdb;
            $query = "SELECT "
            . self::TABLE_PROPOSALS .".id_proposal, "
            . self::TABLE_PROPOSALS .".date_created, "
            . self::TABLE_PROPOSALS .".date_accepted, "
            . self::TABLE_PROPOSALS .".user_create AS prop_user_create, "
            . self::TABLE_PROPOSALS .".user_create_id, "
            . self::TABLE_PROPOSALS .".proposal_value, "
            . self::TABLE_PROPOSALS .".message, "            
            . self::TABLE_PROPOSALS .".id_assign, "
            . self::TABLE_PROPOSALS .".proposal_status, "
            . self::TABLE_USER_INFO .".user_score, "
            . self::TABLE_USER_INFO .".total_earned, "
            . self::TABLE_USER_INFO .".total_assignments, "
            . self::TABLE_TAREAS .".user_create FROM "
            . self::TABLE_PROPOSALS ." JOIN "
            . self::TABLE_TAREAS ." ON "
            . self::TABLE_PROPOSALS .".id_assign="
            . self::TABLE_TAREAS .".id_assign "
            . " JOIN "
            . self::TABLE_USER_INFO ." ON "
            . self::TABLE_PROPOSALS .".user_create_id="
            . self::TABLE_USER_INFO .".id_user"
            . " WHERE "
            . self::TABLE_PROPOSALS .".id_assign= $assign_id AND "
            . self::TABLE_PROPOSALS .".proposal_status != 3 ORDER BY "
            . self::TABLE_PROPOSALS .".date_created ASC";

            $proposals_info = $wpdb->get_results($query);
            
            if (count($proposals_info) > 0) { //We check if the result from the query is empty
                return $proposals_info;
            }
        }
        return 0;
    }

    /*
     * This function returns the proposal by the given ID
     * 
     */
    public function getProposal ($proposal_id) {
        if ($proposal_id) {
            global $wpdb;
            $query = "SELECT "
            . self::TABLE_PROPOSALS .".id_proposal, "
            . self::TABLE_PROPOSALS .".date_created, "
            . self::TABLE_PROPOSALS .".date_accepted, "
            . self::TABLE_PROPOSALS .".user_create AS prop_user_create, "
            . self::TABLE_PROPOSALS .".user_create_id AS prop_user_create_id, "
            . self::TABLE_PROPOSALS .".proposal_value, "
            . self::TABLE_PROPOSALS .".message, "            
            . self::TABLE_PROPOSALS .".id_assign, "
            . self::TABLE_PROPOSALS .".proposal_status, "
            . self::TABLE_USER_INFO .".user_score, "
            . self::TABLE_USER_INFO .".total_earned, "
            . self::TABLE_USER_INFO .".total_assignments, "
            . self::TABLE_TAREAS .".id_assign , "
            . self::TABLE_TAREAS .".money_value, "
            . self::TABLE_TAREAS .".title, "
            . self::TABLE_TAREAS .".description, "
            . self::TABLE_TAREAS .".category, "
            . self::TABLE_TAREAS .".last_update, "
            . self::TABLE_TAREAS .".user_create AS ass_user_create"
            . " FROM "
            . self::TABLE_PROPOSALS ." JOIN "
            . self::TABLE_TAREAS ." ON "
            . self::TABLE_PROPOSALS .".id_assign="
            . self::TABLE_TAREAS .".id_assign "
            . " JOIN "
            . self::TABLE_USER_INFO ." ON "
            . self::TABLE_PROPOSALS .".user_create_id="
            . self::TABLE_USER_INFO .".id_user"
            . " WHERE "
            . self::TABLE_PROPOSALS .".id_proposal= $proposal_id ";
            
            $proposal_info = $wpdb->get_results($query);

            if (count($proposal_info) > 0) { //We check if the result from the query is empty
                return $proposal_info;
            }
        }
        return 0;
    }

    /*
     * This function changes the proposal status to accepted
     * 
     */
    public function acceptProposal($proposal_id){

        $proposalId = absint($proposal_id);

        if ($proposalId > 0) {

            global $wpdb;

            $date = date('Y-m-d H:i:s');
            $query = "UPDATE ". self::TABLE_PROPOSALS ." SET proposal_status = 2, date_accepted = '$date' WHERE id_proposal = $proposalId";

            $proposal = $wpdb->get_var($query);

            return $proposal;
        }
    }
}

?>