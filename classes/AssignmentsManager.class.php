<?php
/*
 * This class manages the CRUD for the assignments and assignment information and it's management on the site
 *
 * Table Name: ap_tareas
 * Table Columns: assign_status, category, date_created, date_finished, description, due_date, user_create_id,
 *                id_assign*, last_update, money_value, offers, title, user_assigned, user_create
 * 
 * Note: Some columns in this table should be created separatelly on their own tables
 *       ->
 */
class AssignmentsManager
{
    const TABLE_TAREAS = "ap_tareas";
    const TABLE_PROPOSALS = "ap_propuestas";
	
	public function __construct() {
        //Code necessary to initialize the class
    }

    /*
     * This function returns an array with all the assignments on the site
     */
    public function allAssigmentsOnSite () {
    }

    /*
     * This function returns an array with all the information of a single Assignment
     */
    public function assignmentInfo ($assign_id) {

        if ($assign_id) {
            global $wpdb;
            $query = "SELECT * FROM ". self::TABLE_TAREAS ." WHERE id_assign = $assign_id";
            $assign_info = $wpdb->get_results($query);
            
            if (count($assign_info) > 0) { //We check if the result from the query is empty
                return $assign_info;
            }
        }

        return 0;
    }

    /*
     * This function returns an array with all the assignments created by an specific user and with the specified status
     * 
     */
    public function allAssignmentsByUser ($user_id, $assign_status) {

        if ($user_id) {
            global $wpdb;
            $query = "SELECT * FROM ". self::TABLE_TAREAS ." WHERE user_create_id = $user_id AND assign_status = $assign_status";
            $assign_info = $wpdb->get_results($query);
            
            if (count($assign_info) > 0) { //We check if the result from the query is empty
                return $assign_info;
            }
        }

        return 0;
    }

    /*
     * This function returns an array with all the assignments assigned to an specific user and with the specified status
     * 
     */
    public function getAllAssignmentsToUser ($user_id, $assign_status) {

        if ($user_id) {
            global $wpdb;
            $query = "SELECT * FROM ". self::TABLE_TAREAS ." WHERE user_assigned = $user_id AND assign_status = $assign_status";
            $assign_info = $wpdb->get_results($query);
            
            if (count($assign_info) > 0) { //We check if the result from the query is empty
                return $assign_info;
            }
        }

        return 0;
    }

    /*
     * This function sets the status of the assignment to deleted
     * In this version no Assignment is eliminated.
     * 
     */
    public function deleteAssignmentByID ($assignment_id) {

        if ($assignment_id) {
            global $wpdb;

            $result = $wpdb->update(
                self::TABLE_TAREAS,
                array(
                    'assign_status' => 3
                    ),
                array(
                    'id_assign' => $assignment_id
                    ),
                array( '%d' ), 
                array( '%d' )
            );

            return $result;
        }

        return 0;
    }

    /*
     * This function returns the assignment to which the given proposal is related
     * 
     */
    public function getAssignmentByProposal ($proposal_id) {
        if ($proposal_id) {
            global $wpdb;
            $query = "SELECT "
            // . self::TABLE_PROPOSALS . ".date_accepted, "
            // . self::TABLE_PROPOSALS . ".date_created, "
            // . self::TABLE_PROPOSALS . ".id_assign, "
            // . self::TABLE_PROPOSALS . ".id_proposal, "
            // . self::TABLE_PROPOSALS . ".message, "
            // . self::TABLE_PROPOSALS . ".proposal_status, "
            // . self::TABLE_PROPOSALS . ".proposal_value, "
            // . self::TABLE_PROPOSALS . ".user_create_id, "
            // . self::TABLE_PROPOSALS . ".user_create, "
            . self::TABLE_TAREAS .".assign_status, "
            . self::TABLE_TAREAS .".category, "
            . self::TABLE_TAREAS .".date_assigned, "
            . self::TABLE_TAREAS .".date_created, "
            . self::TABLE_TAREAS .".date_finished, "
            . self::TABLE_TAREAS .".description, "
            . self::TABLE_TAREAS .".due_date, "
            . self::TABLE_TAREAS .".last_update, "
            . self::TABLE_TAREAS .".money_value, "
            . self::TABLE_TAREAS .".offers, "
            . self::TABLE_TAREAS .".title, "
            . self::TABLE_TAREAS .".user_assigned, "
            . self::TABLE_TAREAS .".user_create, "
            . self::TABLE_TAREAS .".user_create_id FROM "
            . self::TABLE_TAREAS . " JOIN "
            . self::TABLE_PROPOSALS  ." ON "
            . self::TABLE_PROPOSALS .".id_assign="
            . self::TABLE_TAREAS .".id_assign WHERE "
            . self::TABLE_PROPOSALS .".id_proposal = $proposal_id";

            $assigment_info = $wpdb->get_results($query);

            //We check if the result from the query is empty
            if (count($assigment_info) > 0) {
                return $assigment_info;
            }
        }
        return 0;
    }

    
}