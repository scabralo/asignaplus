<?php
/* 
 * This Class manages the CRUD for the messages and messages information
 * 
 * Table Name: ap_messages
 * Table Columns: 
 */
class MessagesManager
{
	const TABLE_TAREAS = "ap_tareas";
    const TABLE_PROPOSALS = "ap_propuestas";
    const TABLE_USER_INFO = "ap_user_info";
    const TABLE_MESSAGES = "ap_messages";

    public function __construct() {
        //Code necessary to initialize the class
    }

    public function allMessagesByProposal ($proposal_id) {
    	if ($proposal_id) {
    		global $wpdb;
    		$query = "SELECT "
            . self::TABLE_MESSAGES .".date_created, "
            . self::TABLE_MESSAGES .".id_message, "
            . self::TABLE_MESSAGES .".user_create_message_id, "
            . self::TABLE_MESSAGES .".user_create_message, "
            . self::TABLE_MESSAGES .".message_content, "
            . self::TABLE_MESSAGES .".id_proposal, "
            . self::TABLE_MESSAGES .".message_status FROM "
            . self::TABLE_MESSAGES ." WHERE "
            . self::TABLE_MESSAGES .".id_proposal = $proposal_id "
            . " ORDER BY "
            . self::TABLE_MESSAGES .".date_created ASC";

            $message_info = $wpdb->get_results($query);

            if (count($message_info) > 0) { //We check if the result from the query is empty
                return $message_info;
            }
    	}
    }

}