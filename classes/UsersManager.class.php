<?php
/*
 * This class manages the CRUD for users and user information and it's management on the site
 *
 * Table Name: ap_user_info
 * Table Columns: id_info*, id_user, total_assignments, total_earned, user_description, user_score, user_status
 * 
 */
class UsersManager
{
	const TABLE_NAME = "ap_user_info";

	public function __construct() {
		//Code necessary to initialize the class
	}

	public function createUser($user_info) {
		//This is currently done using the S2Member plugin and should continue to be this way unless
		//otherwise necessary
		//->Nov 20th, 2015 
	}

	public function readUserInfo($user_id) {

		if ($user_id) {

			$user_info = $wpdb->get_results("SELECT * FROM ". self::TABLE_NAME ." WHERE id_user = $user_id");
			
			if (count($user_info) > 0) { //We check if the result from the query is empty
				return $user_info;
			}
		}

		return 0;

	}

	public function updateUser($user_id, $user_info) {

	}

	public function deleteUser($user_id) {

	}

	
}

?>