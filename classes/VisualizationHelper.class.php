<?php
/*
 * This class will help create the visualization of several elements on the site (users, assignments, etc)
 */
class VisualizationHelper
{
	/*
	 * This fucntion retuns a general view of the assignments array passed as a parameter
	 */
	public function simpleAssignmentsTable ($assign_array) {
		if ($assign_array > 0) {
			$html = '<table>';
			$html .= '<thead><tr>';
			$html .= '<td>Asignación</td>';
			$html .= '<td>Cliente</td>';
			$html .= '<td>Valor</td>';
			$html .= '</tr></thead>';
			$html .= '<tbody>';
			foreach ($assign_array as $assign) {
				$html .= '<tr>';
				$html .= '<td><a href="/detalle-tarea/?id='. $assign->id_assign .'">'. ($assign->title != 0 ? $assign->title: " " ).'</a></td>';
				$html .= '<td>'. ($assign->user_create != 0 ? $assign->user_create : " ") .'</td>';
				$html .= '<td>$'. ($assign->money_value != 0 ? $assign->money_value : " " ) .'</td>';
				$html .= '</tr>';
			}
			unset($assign);

			$html .= '</tbody></table>';

			echo $html;
		} else {
			echo '<p class="empty_list">No se encontraron asignaciones en esta categoría.</p>';
		}
	}
	/*
	 * This function returns a table with the assignments created by the user
	 */
	public function simpleOwnAssignmentsTable ($assign_array) {
		if ($assign_array > 0) {
			$html = '<table>';
			$html .= '<thead><tr>';
			$html .= '<td>Id</td>';
			$html .= '<td>Asignación</td>';
			$html .= '<td>Agente</td>';
			$html .= '<td>Valor</td>';
			$html .= '</tr></thead>';
			$html .= '<tbody>';
			foreach ($assign_array as $assign) {

				$user_assigned_name = " ";

				if ($assign->user_assigned != 0) {
					$user_assigned_obj = get_user_by('id', $assign->user_assigned);
					$user_assigned_name = $user_assigned_obj->user_nicename;
				}

				$html .= '<tr>';
				$html .= '<td>'. $assign->id_assign .'</td>';
				$html .= '<td><a href="/detalle-tarea/?id='. $assign->id_assign .'">'. $assign->title .'</a></td>';
				$html .= '<td>'. $user_assigned_name .'</td>';
				$html .= '<td>$'. ($assign->money_value != 0 ? $assign->money_value : " " ) .'</td>';
				$html .= '</tr>';
			}
			unset($assign);

			$html .= '</tbody></table>';

			echo $html;
		} else {
			echo '<p class="empty_list">No se encontraron asignaciones en esta categoría.</p>';
		}
	}

	/*
	 * This function returns a table with the assignments creates by the user
	 */
	public function simpleProposalsTable ($proposals_array) {
		if ($proposals_array > 0) {

			//format the date 
			$formatter = new IntlDateFormatter('es_ES', IntlDateFormatter::SHORT, IntlDateFormatter::SHORT);
			$formatter->setPattern('MMM dd, Y');

			$html = '<table>';
			$html .= '<thead><tr>';
			$html .= '<td>Id</td>';
			$html .= '<td>Fecha</td>';
			$html .= '<td>Asignación</td>';
			$html .= '<td>Usuario</td>';
			$html .= '</tr></thead>';
			$html .= '<tbody>';
			foreach ($proposals_array as $proposal) {

				//retrieve the date info
				$date_created = ($proposal->date_created != 0 ? new DateTime($proposal->date_created) : " ");

				$html .= '<tr>';
				$html .= '<td>'. $proposal->id_assign .'</td>';
				$html .= '<td>'. $formatter->format($date_created) .'</td>';
				$html .= '<td><a href="/detalle-tarea/?id='. $proposal->id_assign .'">'. ($proposal->title != "" ? $proposal->title : " ") .'</a></td>';
				$html .= '<td><a href="/perfil-usuario/?id='. $proposal->user_create_id .'">'. ($proposal->user_create != "" ? $proposal->user_create : " " ) .'</a></td>';
				$html .= '</tr>';
			}
			unset($proposal);

			$html .= '</tbody></table>';

			echo $html;
		} else {
			echo '<p class="empty_list">No se encontraron propuestas en esta categoría.</p>';
		}
	}

	public function asignaHeader() {
		$html = '';
		$html .= '<header id="page_title_bar">';
		$html .= '<span class="blue_square"></span>' . the_title( '<h1 class="entry-title">', '</h1>', false );
		$html .= '<div id="title_bar_links">';

		if (is_user_not_logged_in()) {
			$html .= '<a href="/wp-login.php">log in</a> / <a href="/wp-login.php?action=register">crear cuenta</a>';
		} else {
			$html .= '<a href="'. wp_logout_url() .'">salir</a>';
		}

		$html .= '</div>';
		$html .= '</header>';

		echo $html;
	}

	/**
   * Gets the formated value of day of the due_date.
   * 
   * @param string $format the format in which the date must be returned. ('dd', 'MMM', 'MMM dd, Y')
   * @param string $language this is the language of the current format, the default value is 'es_ES' for spanish
   * 
   * @return string
   */
  public function getDateFormated($format, $date, $language = 'es_ES') {
  	$formatter = new IntlDateFormatter($language, IntlDateFormatter::SHORT, IntlDateFormatter::SHORT);
		$formatter->setPattern($format);

        $date_to_format = new DateTime($date);

		return $formatter->format($date_to_format);
  }

  public function simpleAllMessages ($messages_array) {
  	if ($messages_array > 0) {

			//format the date 
			$formatter = new IntlDateFormatter('es_ES', IntlDateFormatter::SHORT, IntlDateFormatter::SHORT);
			$formatter->setPattern('MMM dd, Y');

			$current_user = wp_get_current_user();

			$html = '<ul class="messages_list">';

			foreach ($messages_array as $message) {
				$date_created = ($message->date_created != 0 ? new DateTime($message->date_created) : " ");

				// If the current message was created by the current user use this class
				if ($current_user->ID == $message->user_create_message_id) {
					$html .= '<li class="message_left">';
				} else {
					$html .= '<li class="message_right">';
				}
				
				$html .= '<div class="message_wrapper">';

				$html .= '<div class="message_header">';
				$html .= '<span class="user_id">'. $message->user_create_message .' -</span>';
				$html .= '<span class="message_date"> '. $formatter->format($date_created) .'</span>';
				$html .= '</div>';

				$html .= '<div class="message_content">';
				$html .= '<p>'. $message->message_content .'</p>';
				$html .= '</div>';

				$html .= '</div>';

				$html .= '</li>';

			}

			$html .= '</ul>';
			echo $html;

		} else {
			echo '<p class="empty_list">No hay mensajes en esta propuesta, envía el primero!</p>';
		}
  }
}
?>