<?php
class Assignment
{
	const TABLE_TAREAS = "ap_tareas";
    const TABLE_PROPOSALS = "ap_propuestas";

	protected $id = 'value';
	protected $assign_status = '';
	protected $category = '';
	protected $date_created = '';
	protected $date_finished = '';
	protected $description = '';
	protected $due_date = '';
	protected $user_create_id = '';
	protected $id_assign = '';
	protected $last_update = '';
	protected $money_value = '';
	protected $offers = '';
	protected $title = '';
	protected $user_assigned = '';
	protected $user_create = '';

	public function __construct($new_id) {

		if ($new_id) {

			global $wpdb;

			$query = "SELECT * FROM ". self::TABLE_TAREAS ." WHERE id_assign = $new_id";
            $assign_array = $wpdb->get_results($query);

            // var_dump($assign_array);
            
            if (count($assign_array) > 0) { //We check if the result from the query is empty
                $assign_info = $assign_array[0];

                // var_dump($assign_info);
                // echo $this->id;

                $this->id = $assign_info->id_assign;
                $this->assign_status = $assign_info->assign_status;
				$this->category = $assign_info->category;
				$this->date_created = $assign_info->date_created;
				$this->date_finished = $assign_info->date_finished;
				$this->description = $assign_info->description;
				$this->due_date = $assign_info->due_date;
				$this->user_create_id = $assign_info->user_create_id;
				$this->id_assign = $assign_info->id_assign;
				$this->last_update = $assign_info->last_update;
				$this->money_value = $assign_info->money_value;
				$this->offers = $assign_info->offers;
				$this->title = $assign_info->title;
				$this->user_assigned = $assign_info->user_assigned;
				$this->user_create = $assign_info->user_create;

                // echo $this->due_date;
            }
		}
	}


    /**
     * Gets the value of id.
     *
     * @return mixed
     */
    public function getId(){
        return $this->id;
    }

    /**
     * Gets the value of assign_status.
     *
     * @return mixed
     */
    public function getAssign_status(){
        return $this->assign_status;
    }

    /**
     * Gets the value of category.
     *
     * @return mixed
     */
    public function getCategory(){
        return $this->category;
    }

    /**
     * Gets the value of date_created.
     *
     * @return mixed
     */
    public function getDate_created(){
        return $this->date_created;
    }

    /**
     * Gets the value of date_finished.
     *
     * @return mixed
     */
    public function getDate_finished(){
        return $this->date_finished;
    }

    /**
     * Gets the value of description.
     *
     * @return mixed
     */
    public function getDescription(){
        return $this->description;
    }

    /**
     * Gets the value of due_date.
     *
     * @return string
     */
    public function getDue_date(){
        return $this->due_date;
    }

  //   /**
  //    * Gets the formated value of day of the due_date.
  //    * 
  //    * @param string $format the format in which the date must be returned. ('dd', 'MMM', 'MMM dd, Y')
  //    * @param string $language this is the language of the current format, the default value is 'es_ES' for spanish
  //    * 
  //    * @return string
  //    */
  //   public function getDue_date_formated($format, $language = 'es_ES') {
  //   	$formatter = new IntlDateFormatter($language, IntlDateFormatter::SHORT, IntlDateFormatter::SHORT);
		// $formatter->setPattern($format);

  //       $date_to_format = new DateTime($this->due_date);

		// return $formatter->format($date_to_format);
  //   }

    /**
     * Gets the value of user_create_id.
     *
     * @return mixed
     */
    public function getUser_create_id(){
        return $this->user_create_id;
    }

    /**
     * Gets the value of id_assign.
     *
     * @return mixed
     */
    public function getId_assign(){
        return $this->id_assign;
    }

    /**
     * Gets the value of last_update.
     *
     * @return mixed
     */
    public function getLast_update(){
        return $this->last_update;
    }

    /**
     * Gets the value of money_value.
     *
     * @return integer
     */
    public function getMoney_value(){
        return absint($this->money_value);
    }

    /**
     * Gets the value of offers.
     *
     * @return integer
     */
    public function getOffers(){
        return absint($this->offers);
    }

    /**
     * Gets the value of title.
     *
     * @return mixed
     */
    public function getTitle(){
        return $this->title;
    }

    /**
     * Gets the value of user_assigned.
     *
     * @return mixed
     */
    public function getUser_assigned(){
        return $this->user_assigned;
    }

    /**
     * Gets the value of user_create.
     *
     * @return mixed
     */
    public function getUser_create(){
        return $this->user_create;
    }

    /**
     * Checks if an given user has already sent a proposal to the current assignment
     * 
     * @param string 
     * @return boolean
     */
    public function hasUserProposed($user_id){

        $user_id = absint($user_id);

        if ($user_id > 0) {
            
            global $wpdb;
            $query = "SELECT COUNT(*) FROM ". self::TABLE_PROPOSALS ." WHERE id_assign = $this->id_assign AND user_create_id = $user_id";
            $proposals = $wpdb->get_var($query);

            if ($proposals == 0) {
                return false;
            } else {
                return true;
            }
        }

        return true;
    }

    /**
     * Returns how many proposals this 
     * 
     * @return integer
     */
    public function proposalsCount(){
        
        global $wpdb;
        $query = "SELECT COUNT(*) FROM ". self::TABLE_PROPOSALS ." WHERE id_assign = $this->id_assign";
        $proposals = $wpdb->get_var($query);

        return absint($proposals);
    }

    /**
     * Assigns the Assignment to a user 
     * 
     * @return integer
     */
    public function assignAssignment($userId){
        
        $user_id = absint($userId);
        // echo $user_id;

        if ($user_id > 0) {

            global $wpdb;
            
            $date = date('Y-m-d H:i:s');

            $query = "UPDATE ". self::TABLE_TAREAS ." SET user_assigned = $user_id, assign_status = 2, date_assigned = '$date' WHERE id_assign = $this->id_assign";

            // echo $query;
            
            $assignment = $wpdb->get_var($query);

            return absint($assignment);
        }
    }
}
?>