<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 *
 * @package AsignaPlus
 */
?>

	</div><!-- #content -->

	<footer id="colophon" class="site-footer" role="contentinfo">
		<div class="site-info">
			<p>Derechos Reservados Asigna+ <?php echo date('Y');?> * Powered by <a target="_blank" href="http://oryxsd.com/">OryxSD</a></p>
		</div><!-- .site-info -->
	</footer><!-- #colophon -->
</div><!-- #page -->

<?php wp_footer(); ?>

</body>

<script>
	$(document).ready(function() {
		$('ul#menu-main-menu li').prepend("<span></span>");
		$('#masthead .site-branding a').attr('href', '/inicio/');

		$('#materias_select').change(function (evt) {
			var image_location = 'http://asignaplus.com/wp-content/themes/asignaplus/imgs/materias/';
			var materia_actual = $('#materias_select').val();
			$('#materia_image').attr('src',image_location + materia_actual + '.gif');
		});

		$(".numeric").numeric();

		$('#valor_input').focusout(function (evt) {
			var valor = $('#valor_input').val();
			var monto = '';
			if (valor < 10) {
				$('#valor_input').val('10');
				valor = '10';
			};
			monto = 'US$' + valor;
			$('#valor_label').text(monto);

		});

		//****This line prevents the dashboard elements from refreshing the page. Eliminate once those pages are ready.
		//$('#dashboard_area_top a').click(function (evt) { evt.preventDefault(); });
	});
</script>

</html>
