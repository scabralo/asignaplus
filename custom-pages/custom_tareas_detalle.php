<?php
/**
 * Template Name: Detalle Tareas
 */

spl_autoload_register(function ($class) {
	$filepath = realpath (dirname(__FILE__));
	include_once ( $filepath . '/../classes/' . $class . '.class.php');
});

$id_assign = 0;
$user_posted = 0; //The ID of the user that posted the assignment.
$current_user_ID = get_current_user_id();
$current_assign = '';

$v_helper = new VisualizationHelper();

if (isset($_GET['id'])) {
	$id_assign = absint($_GET['id']);
}

if ( $id_assign > 0 ) {
					
	// $assign = $wpdb->get_results("SELECT * FROM ap_tareas WHERE id_assign = $id_assign");
	$current_assign = new Assignment($id_assign);

	if ( $current_assign->getId() <= 0 || $current_assign->getAssign_status() == 3 ) { //We check if the result from the query is empty
		wp_redirect('/tareas/?msg=5'); exit;
	}

} else {
	wp_redirect('/tareas/?msg=5'); exit;
}


get_header(); ?>
	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">
			<!-- Page Custom Content START -->
			
			<?php $v_helper->asignaHeader(); ?>

			<div id="tareas_details_container">
				<?php
					$user_posted = $current_assign->getUser_create_id();
					$is_current_user_creator = ($user_posted == $current_user_ID) ? true:false; //is the current user the one who created this assignment?
				?>
				<div id="assign_det_left_column">
					<div class="metadata_container">
						<small class="publisher"><span>creado por: </span><a href="/agentes/perfil-usuario/?id=<?= $current_assign->getUser_create_id(); ?>" ><?= $current_assign->getUser_create(); ?></a></small>
						<small class="date_published"><span>publicado: </span><?= $v_helper->getDateFormated('MMM dd, Y',$current_assign->getDate_created()); ?></small>
					</div>
					<div id="assign_detail_header" class="clearfix">
						<div class="due_date_container square">
							<p class="due_date_label custom_label">a entregar</p>
							<p class="due_date_month"><?= $v_helper->getDateFormated('MMM',$current_assign->getDue_date()); ?></p>
							<p class="due_date_day"><?= $v_helper->getDateFormated('dd',$current_assign->getDue_date()); ?></p>
						</div> <!-- .due_date_container -->
						<div class="category_container  square">
							<img alt="<?= $current_assign->getCategory(); ?>" src="http://asignaplus.com/wp-content/themes/asignaplus/imgs/materias_amarillos/<?= $current_assign->getCategory(); ?>.gif" />
						</div>
						<div class="money_value_container square">
							<p class="money_value_label custom_label">oferta</p>
							<p class="money_value<?php 
								if (($current_assign->getMoney_value() > 999) && ($current_assign->getMoney_value() <= 1998)) {
									echo ' small';
								} elseif ($current_assign->getMoney_value() >= 1999) {
									echo ' smaller';
								}
							?>"><?= $current_assign->getMoney_value(); ?></p>
						</div><!-- .money_value_container -->
					</div><!-- #assign_header -->
					<div id="assign_detail_content">
						<p id="assign_detail_title" class="assign_title"><strong><?= $current_assign->getTitle(); ?></strong></p>
						<p id="assign_detail_description"><?= $current_assign->getDescription(); ?></p>
						<div id="offer_link_container">
						<?php if (is_user_logged_in() && !$is_current_user_creator && !$current_assign->hasUserProposed($current_user_ID)) : //If the user is logged in then he can send a proposal ?> 
							<a class="offer_link" href="#propuestaModal">ofertar&nbsp;<img src="http://asignaplus.com/wp-content/themes/asignaplus/imgs/asigna_checkmark2.jpg"></a>
						<?php endif; ?>
						</div><!-- #offer_link_container -->
					</div><!-- #assign_detail_content -->
					<?php if (is_user_logged_in() && $is_current_user_creator) : //If the user is logged in and he's the owner of the assignment ?>
						<div class="administration_area">
							<!-- <a href="#" class="edit_assign_link"><i class="fa fa-pencil"></i> editar</a>
							&nbsp; -->
							<a href="#deleteModal" class="delete_assign_link"><i class="fa fa-trash"></i> eliminar</a>
						</div>
					<?php endif; ?>
				</div><!-- #assign_det_left_column -->
				<div id="assign_det_right_column">
					<div id="assign_proposals_header" class="clearfix">
						<div class="proposals_count_container square">
							<p class="proposals_count_label custom_label">propuestas</p>
							<p id="proposal_count"><?= $current_assign->proposalsCount(); ?></p>
						</div><!-- .proposals_count_container -->
					</div><!-- #assign_proposals_header -->
				<?php
 
				$proposalsManager = new ProposalsManager();
				$rows = $proposalsManager->allProposalsByAssignment($id_assign);
				
				if ($rows > 0) {

					global $wp_rewrite;
					$rows_per_page = 50;
					$current = (intval(get_query_var('paged'))) ? intval(get_query_var('paged')) : 1;
	 
					$pagination_args = array(
						'base' => @add_query_arg('paged','%#%'),
						'format' => '',
						'total' => ceil(sizeof($rows)/$rows_per_page),
						'current' => $current,
						'show_all' => false,
						'type' => 'plain',
					);
	 
					if( $wp_rewrite->using_permalinks() ) {
						$pagination_args['base'] = user_trailingslashit( trailingslashit( remove_query_arg('s',get_pagenum_link(1) ) ) . 'page/%#%/', 'paged');
					}
	 
					if( !empty($wp_query->query_vars['s']) ) {
						$pagination_args['add_args'] = array('s'=>get_query_var('s'));
					}
	 
					$start = ($current - 1) * $rows_per_page;
					$end = $start + $rows_per_page;
					$end = (sizeof($rows) < $end) ? sizeof($rows) : $end;
					
					echo '<ul id="proposals_list">';
					for ($i=$start;$i < $end ;++$i ) {
						$row = $rows[$i];

						// var_dump($row);

						$html = '';
						$html .= '<li class="proposal_item clearfix">';
						$html .= '<div class="proposal_top clearfix pointer_cursor closed">';
						$html .= '<div class="proposal_value square"><p class="';
							if ((absint($row->proposal_value) > 999) && (absint($row->proposal_value) <= 1998)) {
								$html .= 'small';
							} elseif (absint($row->proposal_value) >= 1999) {
								$html .= 'smaller';
							}
						$html .=  '">'. absint($row->proposal_value)  .'</p></div>';
						//$html .= '<div class="proposal_value">'. $row->proposal_value .'</div>';
						$html .= '<div class="user_info"><p class="user_name">'. $row->prop_user_create .'</p><p class="user_rating">calificación: '. round($row->user_score, 1, PHP_ROUND_HALF_UP) .'/10</p></div>';
						$html .= '</div>';// .proposal_top
						$html .= '<p class="separator">+</p>';
						$html .= '<div class="proposal_bottom clearfix hidden">';
						$html .= '<div class="user_data"><p class="user_earnings">ganancias: '. $row->total_earned .'</p><p class="total_asignments">'. $row->total_assignments .' asignaciones</p></div>';
						$html .= '<div class="proposal_message"><p>';
						//This if is to verify is the current user is the one who sent the proposal or the one who created the assignment
						if ($is_current_user_creator || $row->user_create_id == $current_user_ID) {
							$html .= $row->message;
						} else {
							$html .= '<small>Esta propuesta sólo es visible para el usuario quien creó la asignación.</small>';
						}
						$html .= '</p></div>';
						if ($is_current_user_creator || $row->user_create_id == $current_user_ID) {
							$html .= '<div class="proposal_detail_link">';
							$html .= '<a href="/detalle-propuesta/?id='. $row->id_proposal .'">Ver Propuesta</a>';
							$html .= '</div>';
						}
						$html .= '</div>';// .proposal_bottom
						$html .= '</li>';
						$html .= '';
						echo $html;
					}
					echo '</ul>';
					echo paginate_links($pagination_args);
				}
				?>
			</div>
			</div><!-- #tareas_details_container -->

			<?php if(is_user_logged_in() && !$is_current_user_creator): ?>
			<div id="propuestaModal" class="modalDialog">
				<div class="display_table">
					<a href="#close" title="Close" class="close">X</a>
					<h2>propuesta</h2>
					<form action="<?php bloginfo('template_url'); ?>/pro/post_proposal.php" method="POST">
						<label>mensaje</label>
						<textarea name="message" rows="7" class="small_margin_bottom" placeholder="Contenido de la propuesta" required></textarea>
						<div id="valor_div">
							<label>valor</label><br>
							<input name="amount" id="valor_input" class="numeric" type=number value="10" min=10 pattern="(\d(,?\d+)*)+(\.\d+)?" required>
							<label id="valor_label"></label>
						</div>
						<div id="submit_div">
							<input type="submit" value="Enviar">
						</div>
						<input type="hidden" name="assign_id" value=<?php echo '"'. $id_assign .'"'?>>
					</form>
				</div><!-- .display_table -->
			</div><!-- #popuestaModal -->
			<?php endif; ?>

			<?php if(is_user_logged_in() && $is_current_user_creator): ?>
			<div id="deleteModal" class="modalDialog">
				<div class="display_table">
					<a href="#close" title="Close" class="close">X</a>
					<!-- <h2>¿eliminar?</h2> -->
					<h2>¿Desea eliminar esta asignación?</h2>
					<div class="row options">
						<div class="col-sm-6">
							<a href="<?php bloginfo('template_url'); ?>/pro/del_assignment.php?id=<?php echo $id_assign ?>">Eliminar</a>
						</div>
						<div class="col-sm-6">
							<a href="#close" title="Close">Cancelar</a>
						</div>
					</div>
				</div><!-- .display_table -->
			</div><!-- #popuestaModal -->
			<?php endif; ?>
			<!-- Page Custom Content FINISH -->
		</main><!-- #main -->
	</div><!-- #primary -->
	<script src="http://asignaplus.com/wp-content/themes/asignaplus/js/proposals_manager.js"></script>
<?php get_footer(); ?>