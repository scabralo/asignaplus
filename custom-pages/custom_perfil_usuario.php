<?php
/**
 * Template Name: Perfil Usuario
 */
spl_autoload_register(function ($class) {
	$filepath = realpath (dirname(__FILE__));
	include_once ( $filepath . '/../classes/' . $class . '.class.php');
});

$v_helper = new VisualizationHelper();

get_header(); ?>
	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">
			<!-- Page Custom Content START -->
			
			<?php $v_helper->asignaHeader(); ?>

			<div id="user_details_container" class="clearfix">
				<?php
				$error_msg = ''; //Number of the current error mesage
				/** Get the assignment ID from the $_GET variable
				  * Check that the ID is actually a number and positive ABSINT()
				  * If the id is not a number show an error message
				  * If the id is a number but there is no record of an assignment with that number show the error message
				  * Get the assignation info from the DB
				  */
				$id_user = absint($_GET['id']);

				$wp_user_info = get_userdata($id_user);
				$users = $wpdb->get_results("SELECT * FROM ap_user_info WHERE id_user = $id_user");
				$user_info = $users[0];

				$formatter = new IntlDateFormatter('es_ES', IntlDateFormatter::SHORT, IntlDateFormatter::SHORT);
				$formatter->setPattern('MMM, Y');//Format for the Due Date field

				$registered_date = new DateTime($wp_user_info->user_registered);
				// $date_created = new DateTime($assign->date_created);

				//***************************************

				$assignManager = new AssignmentsManager();
				?>
				
				<!-- Notifications Area -->

				<div id="user_info_top_panel" class="clearfix">
					<?php
					$user_score = round($user_info->user_score, 1, PHP_ROUND_HALF_UP);
					$user_total_earned = number_format($user_info->total_earned, 2, '.', ',');

					$html = '';
					$html .= '<div id="user_score"><p>'. $user_score .'</p></div>';//User Score
					$html .= '<div id="user_name">';
					$html .= '<p id="user_login">'. $wp_user_info->user_login .'</p>';
					$html .= '<p id="registered_date">agente desde<br><span>'. $formatter->format($registered_date) .'</span></p>';
					$html .= '</div>';//#user_name
					$html .= '<div id="total_earned"><p id="total_earned_number">$'. $user_total_earned .'</p><p>ganancias</p></div>';
					echo $html;
					?>
				</div><!-- #user_info_top_panel -->
				<div class="user_info_tabs">
					<div id="tabs">
						<ul>
							<li><a href="#tabs-info">Información</a></li>
							<li><a href="#tabs-current">Asignaciones Actuales</a></li>
							<li><a href="#tabs-previous">Asignaciones Anteriores</a></li>
							<li><a href="#tabs-proposals">Propuestas</a></li>
							<li><a href="#tabs-payments">Pagos</a></li>							
						</ul>
						<div id="tabs-info">
							<p>Proin elit arcu, rutrum commodo, vehicula tempus, commodo a, risus. Curabitur nec arcu. Donec sollicitudin mi sit amet mauris. Nam elementum quam ullamcorper ante. Etiam aliquet massa et lorem. Mauris dapibus lacus auctor risus. Aenean tempor ullamcorper leo. Vivamus sed magna quis ligula eleifend adipiscing. Duis orci. Aliquam sodales tortor vitae ipsum. Aliquam nulla. Duis aliquam molestie erat. Ut et mauris vel pede varius sollicitudin. Sed ut dolor nec orci tincidunt interdum. Phasellus ipsum. Nunc tristique tempus lectus.</p>
						</div>
					    <div id="tabs-current"><!-- Asignaciones Actuales -->

						  	<h2>Propias</h2>
						    <?php
							    $assign_created = $assignManager->allAssignmentsByUser($id_user, "1");
							    $assign_assigned = $assignManager->getAllAssignmentsToUser($id_user, "1");

							    $v_helper = new VisualizationHelper();

							    $v_helper->simpleOwnAssignmentsTable($assign_created);
						    ?>

						    <h2>Asignadas</h2>
						    <?php
							    $v_helper->simpleAssignmentsTable($assign_assigned);
						    ?>

					    </div>
					    <div id="tabs-previous">
					    	<div>
							    <h2>Propias</h2>
							    <?php
								    $assign_created_past = $assignManager->allAssignmentsByUser($id_user, "2");
								    $assign_assigned_past = $assignManager->getAllAssignmentsToUser($id_user, "2");

								    $v_helper->simpleOwnAssignmentsTable($assign_created_past);
							    ?>
						    </div>

						    <div>
								<h2>Asignadas</h2>
								<?php
									$v_helper->simpleOwnAssignmentsTable($assign_assigned_past);
								?>
							</div>
					    </div>
					    <div id="tabs-proposals">
					    	 <h2>Activas</h2>
					    	 <?php
						    	 $proposalsManager = new ProposalsManager();
						    	 $proposals_current = $proposalsManager->allProposalsByUser($id_user, "1");

						    	 $v_helper->simpleProposalsTable($proposals_current);
					    	 ?>
					    </div>
					    <div id="tabs-payments">
					    	<div>
							    <h2>Pendientes</h2>
							    <?php
								    
							    ?>
						    </div>

						    <div>
								<h2>Depositados</h2>
								<?php
									
								?>
							</div>
					    </div>
					</div>
				</div>
			</div>
			<!-- Page Custom Content FINISH -->
		</main><!-- #main -->
	</div><!-- #primary -->
<?php get_footer(); ?>