<?php
/**
 * Template Name: Tareas
 */
spl_autoload_register(function ($class) {
	$filepath = realpath (dirname(__FILE__));
	include_once ( $filepath . '/../classes/' . $class . '.class.php');
});

$v_helper = new VisualizationHelper();

get_header(); ?>
	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">
			<!-- Page Custom Content START -->
			<?php 
				$v_helper->asignaHeader();
			?>
			
			<div id="tareas_list_container">
				<?php
				// Test the use of paginate_links
 
				$rows_per_page = 10;
				$current = (intval(get_query_var('paged'))) ? intval(get_query_var('paged')) : 1;
 
				// $rows is the array that we are going to paginate.
				$rows = $wpdb->get_results("SELECT * FROM ap_tareas WHERE assign_status=1 ORDER BY date_created DESC");
 
				global $wp_rewrite;
 
				$pagination_args = array(
				 'base' => @add_query_arg('paged','%#%'),
				 'format' => '',
				 'total' => ceil(sizeof($rows)/$rows_per_page),
				 'current' => $current,
				 'show_all' => false,
				 'type' => 'plain',
				);
 
				if( $wp_rewrite->using_permalinks() )
				 $pagination_args['base'] = user_trailingslashit( trailingslashit( remove_query_arg('s',get_pagenum_link(1) ) ) . 'page/%#%/', 'paged');
 
				if( !empty($wp_query->query_vars['s']) )
				 $pagination_args['add_args'] = array('s'=>get_query_var('s'));
 
				echo paginate_links($pagination_args);
 
				$start = ($current - 1) * $rows_per_page;
				$end = $start + $rows_per_page;
				$end = (sizeof($rows) < $end) ? sizeof($rows) : $end;

				//setlocale(LC_TIME|LC_CTYPE,'es-ES');
				$formatter = new IntlDateFormatter('es_ES', IntlDateFormatter::SHORT, IntlDateFormatter::SHORT);
				$formatter->setPattern('MMM');//Format for the Due Date field

				$formatter2 = new IntlDateFormatter('es_ES', IntlDateFormatter::SHORT, IntlDateFormatter::SHORT);
				$formatter2->setPattern('dd');

				$formatter3 = new IntlDateFormatter('es_ES', IntlDateFormatter::SHORT, IntlDateFormatter::SHORT);
				$formatter3->setPattern('MMM dd, Y');//Format for the Date Created field

				$proposals_count = 0;

				echo '<ul>';
				for ($i=$start;$i < $end ;++$i ) {
					$row = $rows[$i];
					
					$proposals_count = $wpdb->get_var( "SELECT COUNT(*) FROM ap_propuestas WHERE id_assign = $row->id_assign" );

					$due_date = new DateTime($row->due_date);
					$date_created = new DateTime($row->date_created);

					$html = '';
					$html .= '<li class="tarea_list_item custom_slow_list clearfix">';
					$html .= '<div class="due_date_container square">';
					$html .= '<p class="due_date_label custom_label">a entregar</p>'; //.due_date_label
					$html .= '<p class="due_date_month">'. $formatter->format($due_date) .'</p>';
					$html .= '<p class="due_date_day">'. $formatter2->format($due_date) .'</p>';
					$html .= '</div>'; //.due_date_container
					$html .= '<div class="title_container">';
					$html .= '<p class="assign_title"><a href="/detalle-tarea/?id='. $row->id_assign .'">'. $row->title .'</a></p>';
					$html .= '<div class="metadata_container">';
					$html .= '<small class="publisher"><span>creado por:</span> <a href="/agentes/perfil-usuario/?id='. $row->user_create_id .'">'. $row->user_create .'</a></small>';
					$html .= '<small class="date_published"><span>publicado:</span> '. $formatter3->format($date_created) .'</small>';
					$html .= '</div>'; //.metadata_container
					$html .= '</div>'; //.title_container
					$html .= '<div class="money_value_container square">';
					$html .= '<p class="money_value_label custom_label">oferta</p>';
					$html .= '<p class="money_value';
					if ((absint($row->money_value) > 999) && (absint($row->money_value) <= 1998)) {
						$html .= ' small';
					} elseif (absint($row->money_value) >= 1999) {
						$html .= ' smaller';
					}
					$html .=  '">'. absint($row->money_value)  .'</p>';
					$html .= '</div>';//.money_value_container
					$html .= '<div class="category_container  square"><img alt="'. $row->category .'" src="http://asignaplus.com/wp-content/themes/asignaplus/imgs/materias_amarillos/'. $row->category.'.gif"></div>';
					$html .= '<div class="offers_container square">';
					$html .= '<p class="offers_label custom_label">propuestas</p>';
					$html .= '<p class="offers">'. absint($proposals_count) .'</p>';
					$html .= '</div>';
					$html .= '<a class="offer_link" href="/detalle-tarea/?id='. $row->id_assign .'">ofertar<img src="http://asignaplus.com/wp-content/themes/asignaplus/imgs/asigna_checkmark1.png"></a>';
					$html .= '</li>'; //.tarea_list_item
					echo $html;
				}
				echo '</ul>';
				echo paginate_links($pagination_args);
				?>
			</div><!-- #tareas_list_container -->
			<!--<script src="http://asignaplus.com/wp-content/themes/asignaplus/js/slow_list.js"></script>-->
			<!-- Page Custom Content FINISH -->
		</main><!-- #main -->
	</div><!-- #primary -->
<?php get_footer(); ?>