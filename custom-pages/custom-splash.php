<?php
/**
 * Template Name: Splash Screen
 */

get_header(); ?>
	<!-- Page Custom Content START -->
	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">
			<img id="splash_screen_logo" class="custom_slow_list" src="<?php bloginfo('template_url'); ?>/imgs/asigna_logo1.png" alt="AsignaPlus Logo"/>
			<ul id="splash_list">
				<li class="splash_margin_right custom_slow_list"><span class="yellow_box box splash_abc_text"><p>1</p></span><p class="splash_steps">publica tu asignaci&oacute;n</p></li>
				<li class="splash_margin_right custom_slow_list"><span class="blue_box box splash_abc_text"><p>2</p></span><p class="splash_steps">otorga un valor</p></li>
				<li class="splash_margin_right custom_slow_list"><span class="yellow_box box splash_abc_text"><p>3</p></span><p class="splash_steps">escoge qui&eacute;n la realiza</p></li>
				<li class="custom_slow_list"><span class="blue_box box splash_abc_text"><p>4</p></span><p class="splash_steps">paga cuando est&eacute; lista!</p></li>
			</ul>
			<a href="/inicio/" id="splash_entrar_link" class="custom_slow_list">entrar<img src="<?php bloginfo('template_url'); ?>/imgs/asigna_checkmark1.png"></a>
		</main><!-- #main -->
	</div><!-- #primary -->
	<!--<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/slow_list.js"></script>-->
	<!-- Page Custom Content FINISH -->
<?php get_footer(); ?>
