<?php
/**
 * Template Name: Preguntas Frecuentes
 */

get_header(); ?>
	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">
			<!-- Page Custom Content START -->
			<header id="page_title_bar" >
				<span class="blue_square"></span><?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
				<div id="title_bar_links">
				<?php
				if (is_user_not_logged_in()) {
					echo '<a href="/wp-login.php">log in</a> / <a href="/wp-login.php?action=register">crear cuenta</a>';
				} else {
					echo '<a href="'. wp_logout_url() .'">salir</a>';
				}
				?>
				</div>
			</header><!-- #page_title_bar -->
			<div id="preguntas_frecuentes_container">
				<?php while ( have_posts() ) : the_post(); ?>
					<?php get_template_part( 'content', 'page' ); ?>
				<?php endwhile; // end of the loop. ?>
			</div><!-- #preguntas_frecuentes_container -->
			<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/faqs_effects.js"></script>
			<!-- Page Custom Content FINISH -->
		</main><!-- #main -->
	</div><!-- #primary -->
<?php get_footer(); ?>