<?php
/**
 * Template Name: Detalle Propuesta
 */
spl_autoload_register(function ($class) {
	$filepath = realpath (dirname(__FILE__));
	include_once ( $filepath . '/../classes/' . $class . '.class.php');
});

$proposal_id = 0;
$proposal_info = '';
$assign_manager = '';
$assign_info = '';
$current_assign = '';

if (isset($_GET['id'])) {
	$proposal_id = absint($_GET['id']);
}

if ($proposal_id > 0) {
	
	$v_helper = new VisualizationHelper();
	$prop_manager = new ProposalsManager();
	$msg_manager = new MessagesManager();
	$assign_manager = new AssignmentsManager();

	$current_user_ID = get_current_user_id();
	$assign_info = $assign_manager->getAssignmentByProposal($proposal_id);
	$msg_array = $msg_manager->allMessagesByProposal($proposal_id);
	$proposal_info = $prop_manager->getProposal($proposal_id);

	$is_current_user_creator = ($assign_info[0]->user_create_id == $current_user_ID) ? true:false; //is the current user the one who created this assignment?
	$is_proposer_assignee = ($assign_info[0]->user_assigned == $proposal_info[0]->prop_user_create_id) ? true:false; //is the user to whom this assignment was assigned the same user that sent this proposal?

	if ($proposal_info == 0) {
		wp_redirect('/tareas/?msg=10'); exit;
	}

} else {
	wp_redirect('/tareas/?msg=10'); exit;
}


get_header(); ?>
	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">
			<!-- Page Custom Content START -->
			<?php
				$v_helper->asignaHeader();
			?>
			<div id="assign_det_left_column">
				<div class="metadata_container">
					<small class="publisher"><span>creado por: </span><a href="/agentes/perfil-usuario/?id=<?= $proposal_info[0]->prop_user_create_id; ?>" ><?= $proposal_info[0]->prop_user_create; ?></a></small>
					<small class="date_published"><span>publicado: </span><?= $v_helper->getDateFormated('MMM dd, Y', $proposal_info[0]->date_created); ?></small>
				</div>
				<div id="assign_detail_header" class="clearfix proposal_header"><h2>Propuesta</h2></div>
				<div id="assign_detail_content">
					<p id="assign_detail_title" class="assign_title"><strong><?= $proposal_info[0]->message; ?></strong></p>
				</div><!-- #assign_detail_content -->
				<div class="administration_area">
					<?php
					/*
					 * We need to check is the user is the owner of the assignement as an overall if statement,
					 * inside that if statement we'll check the status of the assignment and based on that we'll show different options:
					 * 1) Assign assignment to current proposer (Change Assignement Status)
					 * 2) Accept deliverables from this proposer, only if this is the user 
					 *    to whom the assignment was assigned in first place (Change Assignement Status)
					 */

					if ($is_current_user_creator) {

						switch ($assign_info[0]->assign_status) {
							// Assignment created but still not assigned to anyone
							case '1':
								?>
								<a id="publicar" href="#acceptProposal"><i class="fa fa-check" aria-hidden="true"></i>Aceptar Propuesta</a>
								<?php
								break;

							// Assignment assigned to user
							case '2':
								?>
								<a id="publicar" href="#acceptDeliverables"><i class="fa fa-money" aria-hidden="true"></i>Liberar Fondos</a>
								<?php
								break;
							
							default:
								# code...
								break;
						}
					}
					?>
				</div>
			</div><!-- #assign_det_left_column -->
			<div id="assign_det_right_column">
				<div id="assign_proposals_header" class="clearfix messages_header">
					<h2>Mensajes</h2>
				</div><!-- #assign_proposals_header -->
				<div class="message_list_container">
					<?php
						/**
						 * Here we are going to add the list of messages sent between users for this proposal
						 */
						$v_helper->simpleAllMessages($msg_array);
					?>
				</div>
				<div class="message_form_container">
					<form action="<?php bloginfo('template_url'); ?>/pro/post_message.php" method="POST">
						<textarea name="message" rows="3" class="small_margin_bottom" placeholder="Mensaje" required></textarea>
						<div id="submit_div">
							<input type="submit" value="Enviar">
						</div>
						<input type="hidden" name="id_proposal" value=<?php echo '"'. $proposal_info[0]->id_proposal .'"'?>>
					</form>
				</div>
			</div><!-- #assign_det_right_column -->
			<?php
			if ($is_current_user_creator) {
			?>
				<div id="acceptProposal" class="modalDialog confirmation-modal">
					<div class="display_table">
					<a href="#close" title="Close" class="close">X</a>
					<!-- <h2>¿eliminar?</h2> -->
					<h2>¿Desea aceptar esta propuesta?</h2>
					<div class="row options">
						<div class="col-sm-6">
							<a href="<?php bloginfo('template_url'); ?>/pro/accept_proposal.php?id=<?php echo $proposal_info[0]->id_proposal ?>">Aceptar</a>
						</div>
						<div class="col-sm-6">
							<a href="#close" title="Close">Cancelar</a>
						</div>
					</div>
				</div><!-- .display_table -->
				</div><!-- #publicarModal -->
			<?php
			}
			?>

			<?php
			if ($is_proposer_assignee) {
			?>
				<div id="acceptDeliverables" class="modalDialog confirmation-modal">
					<div class="display_table">
					<a href="#close" title="Close" class="close">X</a>
					<!-- <h2>¿eliminar?</h2> -->
					<h2>¿Desea liberar los fondos?</h2>
					<p>Al hacer esto la asignación se considera terminada.</p>
					<div class="row options">
						<div class="col-sm-6">
							<a href="<?php bloginfo('template_url'); ?>/pro/pay_assignment.php?id=<?php echo $proposal_info[0]->id_proposal ?>">Aceptar</a>
						</div>
						<div class="col-sm-6">
							<a href="#close" title="Close">Cancelar</a>
						</div>
					</div>
				</div><!-- .display_table -->
				</div><!-- #publicarModal -->
			<?php
			}
			?>
			<!-- Page Custom Content FINISH -->
		</main><!-- #main -->
	</div><!-- #primary -->
<?php get_footer(); ?>