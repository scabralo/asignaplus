<?php
/**
 * Template Name: Inicio
 */

get_header(); ?>
	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">
			<!-- Page Custom Content START -->
			<div id="dashboard_area_top" class="clearfix">
				<ul>
					<li id="log_in"><div class="grey_box dashboard_box">
						<?php 
							if(is_user_not_logged_in()) { dynamic_sidebar( 'loginmenu-1' ); }
							else { 
								$current_user = wp_get_current_user();
								$html = '';
								$html .= '<div id="logged_user_message">';
								$html .= '<p id="greeting">Bienvenido</p>';
								$html .= '<a href="/agentes/perfil-usuario/?id='. absint($current_user->ID) .'">';
								$html .= '<p id="current_user_display_name">' . sanitize_user( S2MEMBER_CURRENT_USER_LOGIN) . '</p>';
								$html .= '</a>';
								//$html .= '<a href="#" onclick="window.open("http://asignaplus.com/?s2member_profile=1", "_popup", "width=600,height=400,left=100,screenX=100,top=100,screenY=100,location=0,menubar=0,toolbar=0,status=0,scrollbars=1,resizable=1"); return false;">Modify Profile</a>';
								$html .= '<a id="modify_account_link" href="#" onclick="window.open(';
								$html .= "'http://asignaplus.com/?s2member_profile=1', '_popup', 'width=600,height=400,left=100,screenX=100,top=100,screenY=100,location=0,menubar=0,toolbar=0,status=0,scrollbars=1,resizable=1');";
								$html .= 'return false;">(modificar perfil)</a>';
								$html .= '<div><a id="logout_link" href="' . wp_logout_url() . '">salir</a></div>';
								$html .= '</div>';
								$html .= '';
								echo $html;
								//echo '<div id="logged_user_message"><p id="greeting">Bienvenido</p><a href="/agentes/perfil-usuario/?id='. absint($current_user->ID) .'"><p id="current_user_display_name">' . sanitize_user( S2MEMBER_CURRENT_USER_LOGIN) . '</p></a><a id="logout_link" href="' . wp_logout_url() . '">salir</a></div>';
							}
						?>
					</div><p class="dashboard_text">log in</p></li>
					<li id="publicar" class="separator">
						<?php if (is_user_logged_in()): ?>
							<a id="publicar" href="#publicarModal">
						<?php else: ?>
							<a id="publicar" href="#">
						<?php endif; ?>
								<div class="yellow_box dashboard_box"></div>
								<p class="dashboard_text">publicar</p>
							</a>
					</li>
					<li id="tareas"><a id="tareas" href="/tareas"><div class="blue_box dashboard_box"></div><p class="dashboard_text">tareas</p></a></li>
					<li id="agentes"><a id="agentes"href="/agentes/"><div class="yellow_box dashboard_box"></div><p class="dashboard_text">agentes</p></a></li>
					<li id="stats"class="separator"><a id="stats" href="#"><div class="blue_box dashboard_box"></div><p class="dashboard_text">stats</p></a></li>
					<li id="materias"><a id="materias" href="#"><div class="grey_box dashboard_box"></div><p class="dashboard_text">materias</p></a></li>
				</ul>
			</div>
			<?php if(is_user_logged_in()):?>
			<div id="publicarModal" class="modalDialog">
				<div class="display_table">
					<a href="#close" title="Close" class="close">X</a>
					<h2>publicar asignación</h2>
					<form action="<?php bloginfo('template_url'); ?>/pro/post_assignment.php" method="POST">
						<label>título</label><br>
						<input id="title_input" placeholder="título de la asignación" type="text" name="titulo" class="small_margin_bottom" required><br>
						<label>descripción</label>
						<textarea name="descripcion" rows="7" class="small_margin_bottom" placeholder="Descripcion de la asignacion" required></textarea>
						<div id="materia_div">
							<label>materia</label><br>
							<select id="materias_select" name="materia" class="small_margin_bottom" required>
								<option value="Otra">Otra</option>
								<option value="Arte">Arte</option>
								<option value="Biologia">Biología</option>
								<option value="Clases">Clases</option>
								<option value="Ensayos">Ensayos</option>
								<option value="Espanol">Español</option>
								<option value="Finanzas">Finanzas</option>
								<option value="Geografia">Geografía</option>
								<option value="Historia">Historia</option>
								<option value="Idiomas">Idiomas</option>
								<option value="Literatura">Literatura</option>
								<option value="Matematicas">Matemáticas</option>
								<option value="Psicologia">Psicología</option>
								<option value="Quimica">Química</option>
								<option value="Software">Software</option>
							</select><br>
							<img id="materia_image" src="<?php bloginfo('template_directory'); ?>/imgs/materias/empty.gif">
						</div>
						<div id="vencimiento_div">
							<label>vencimiento</label><br>
							<input name="fecha_vencimiento" type=date step=1 min=<?php $date = new DateTime('today'); echo $date->format('Y-m-d'); ?> required><label id="vencimiento_label"></label>
						</div>
						<div id="valor_div">
							<label>valor</label><br>
							<input name="monto" id="valor_input" class="numeric" type=number value="10" min=10 max=9999 pattern="(\d(,?\d+)*)+(\.\d+)?" required><br>
							<label id="valor_label"></label>
						</div>
						<div id="submit_div">
							<input type="submit" value="Publicar">
						</div>
					</form>
				</div><!-- .display_table -->
			</div><!-- #publicarModal -->
			<?php endif; ?>
			<!-- <div id="dashboard_area_bottom" class="clearfix">
				<div id="top_rated" class="dashboard_bottom_container">
					<h2>top rated</h2>
					<ul class="dashboard_bottom_inner_list">
						<li class="white_bottom_border" ><span class="toprated_img_cont"><img src="<?php //bloginfo('template_url'); ?>/imgs/math_white.png"></span><p class="dashboard_text">matemáticas<br/> 81% de posts</p></li>
						<li class="white_bottom_border" ><span class="toprated_img_cont"><img src="<?php //bloginfo('template_url'); ?>/imgs/profile_white.png"></span><p class="dashboard_text">pachybanks<br/> 17 posts</p></li>
						<li class="white_bottom_border" ><span class="toprated_img_cont"><img src="<?php //bloginfo('template_url'); ?>/imgs/cash_white.png"></span><p class="dashboard_text">scabral1<br/> US$11,248.00</p></li>
						<li><span class="toprated_img_cont"><img src="<?php //bloginfo('template_url'); ?>/imgs/checkmark_white.png"></span><p class="dashboard_text">scabral1<br/> 94% rating</p></li>
					</ul>
				</div> --><!-- #TOP_RATED -->
				<!-- <div id="posteo_rapido" class="dashboard_bottom_container separator">
					<h2>posteo r&aacute;pido</h2>
					<div>
						<form action="#" id="quick_post">
							<input id="materia" type="text" name="materia" placeholder="materia">
							<input id="fecha" type="text" name="fecha_entrega" placeholder="fecha de entrega">
							<input id="desc" type="text" name="descripcion" placeholder="descripcion de la tarea">
							<input id="oferta" type="text" name="oferta" placeholder="oferta">
							<a id="quickpost_link" href="#">post</a>
						</form>
					</div>
				</div> --><!-- #POSTEO_RAPIDO -->
				<!-- <div id="recientes" class="dashboard_bottom_container">
					<h2>recientes</h2>
					<div>
					</div>
				</div> --><!-- #RECIENTES -->
			<!-- </div> -->
			<!-- Page Custom Content FINISH -->
		</main><!-- #main -->
	</div><!-- #primary -->
<?php get_footer(); ?>