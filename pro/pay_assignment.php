<?php
/**
 * This file manages the payments of assignments on the site.
 *
 * @author 		Oryx,Software Development
 * @version     1.0.0
 */
require_once( explode( "wp-content" , __FILE__ )[0] . "wp-load.php" );

// Make sure $_POST is set and all required fields are set too
if (empty($_POST)) {

	wp_redirect('/inicio/'); exit;
}

// Make sure all of the required fields are populated
if (empty($_POST['id']) ) {
	
	wp_redirect('/inicio/?msg=18'); exit;
}

$proposal_id = absint($_POST['id']);

date_default_timezone_set("America/Santo_Domingo");

/*
 * Here we'll take care of the payment process once the db table and the class are ready
 */

wp_redirect('/detalle-propuesta/?id='. $proposal_id); exit;