<?php
/**
 * This file manages the acceptance of proposals on the site.
 *
 * @author    Oryx,Software Development
 * @version     1.0.0
 */
require_once( explode( "wp-content" , __FILE__ )[0] . "wp-load.php" );
spl_autoload_register(function ($class) {
  $filepath = realpath (dirname(__FILE__));
  include_once ( $filepath . '/../classes/' . $class . '.class.php');
});


// TODO: Make sure that the ID field is on the GET variable
// TODO: If the ID variable is not on the GET Variable then take the user to the Tareas Page and show error message
// TODO: If the ID variable is on the GET Variable but the user is not authorized to change it then go to that assignment page and show an error message.
if( empty($_GET['id']) || absint($_GET['id']) <= 0 ) {

  //Notify the user that there has been an error eliminating the assignment
  wp_redirect('/tareas/&'); exit;
}

// The user accepts the proposal
// Two things need to happen:
// - The assignment status and info must change to assigned
//  * We need the assignement id
//  * We need the id of the user to
// - The proposal status and info must change to accepted



$idProposal = absint($_GET['id']);
// echo "idProposal: ". $idProposal ."<br>";

$currentUserID = get_current_user_id();
// echo "currentUserID: ". $currentUserID ."<br>";

$proposalManager = new ProposalsManager();

$currentProposalArray = $proposalManager->getProposal($idProposal);
$currentProposal = $currentProposalArray[0];

// var_dump($currentProposal);

$currentAssign = new Assignment($currentProposal->id_assign);

// var_dump($currentAssign);

$is_current_user_creator = ($currentAssign->getUser_create_id() == $currentUserID) ? true:false; //is the current user the one who created this assignment?

if ($is_current_user_creator) {
  
  $prop_user_create_id = $currentProposal->prop_user_create_id;
  $currentAssign->assignAssignment($prop_user_create_id);
  $proposalManager->acceptProposal($idProposal);

  wp_redirect('/detalle-propuesta/?id='. absint($idProposal) .'&msg=16'); exit;

  /*
   * Here we need a final step to update the Price of the assignement to that of the
   * Accepted proposal, that will save us some time .
   */

} else {

  wp_redirect('/detalle-propuesta/?id='. absint($idProposal) .'&msg=17'); exit;

}

