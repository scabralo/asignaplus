<?php
/**
 * This file manages the posting of messages on the site.
 *
 * @author 		Oryx,Software Development
 * @version     1.0.0
 */
require_once( explode( "wp-content" , __FILE__ )[0] . "wp-load.php" );

// Make sure $_POST is set and all required fields are set too
if (empty($_POST)) {
	
	//return to Inicio and show and Error MSG: "La propuesta no pudo ser publicada, intentelo de nuevo."
	wp_redirect('/inicio/'); exit;
}

// Make sure all of the required fields are populated
if (empty($_POST['message']) || 
	empty($_POST['id_proposal'])) {
	
	if (!empty($_POST['assign_id'])) { //if the id is available return to that page and show message
		wp_redirect('/detalle-propuesta/?id='. absint($_POST['id_proposal']) .'&msg=6'); exit;
	} else { //if the id is not available go back to tareas and show message
		wp_redirect('/tareas/?msg=6'); exit;
	}
}

date_default_timezone_set("America/Santo_Domingo");

//Auto Generated Values
$fecha_actual = date("Y-m-d H:i:s");
$current_user = wp_get_current_user();

//Variables to hold the values without being sanitized
$tainted_message = $_POST['message'];
$tainted_proposal_id = $_POST['id_proposal'];

//Creating the new message
$wpdb->insert(
	'ap_messages',
	array( 
		'date_created' => $fecha_actual,
		'user_create_message' =>  $current_user->user_login,
		'user_create_message_id' =>  $current_user->ID,
		'message_content' => $tainted_message,
		'id_proposal' => $tainted_proposal_id
	)
);

if ($wpdb->insert_id) {
	//Notify the user that the message has been created successfuly
	wp_redirect('/detalle-propuesta/?id='. absint($tainted_proposal_id)); exit;

} elseif (!$wpdb->insert_id) {
	//Notify the user that there has been an error creating the message
	wp_redirect('/detalle-propuesta/?id='. absint($tainted_proposal_id) .'&msg=15'); exit;
}