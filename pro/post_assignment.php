<?php
/**
 * This file manages the posting of assignments on the site.
 *
 * @author 		Oryx,Software Development
 * @version     1.0.0
 */
require_once( explode( "wp-content" , __FILE__ )[0] . "wp-load.php" );

// Make sure $_POST is set and all required fields are set too
if (empty($_POST)) {
	
	//return to Inicio and show and Error MSG: "La asignacion no pudo ser publicada, intentelo de nuevo."
	wp_redirect('/tareas/?msg=2'); exit;
}

// Make sure all of the required fields are populated
if (empty($_POST['titulo']) || 
	empty($_POST['descripcion']) || 
	empty($_POST['materia']) || 
	empty($_POST['fecha_vencimiento']) || 
	empty($_POST['monto'])) {
	
	//if any of the fields are empty go back to INICIO and show an Error MSG: 
	wp_redirect('/inicio/?msg=2'); exit;
}

// IMPORTANT: Check Length and Type of every field, if something is wrong go back and show Error MSG

date_default_timezone_set("America/Santo_Domingo");

//Variables to hold the values before being sanitized
$tainted_titulo=$_POST['titulo'];
$tainted_descripcion=$_POST['descripcion'];
$tainted_materia=$_POST['materia'];
$tainted_fecha_vencimiento=$_POST['fecha_vencimiento'];
$tainted_monto=$_POST['monto'];

//Variable Sanitation Phase
//$sanitized_titulo = sanitize_text_field($tainted_titulo);
//$sanitized_descripcion = sanitize_text_field($tainted_descripcion);
//$sanitized_materia = sanitize_text_field($tainted_materia); //This field is equivalent to CATEGORY
//$sanitized_fecha = sanitize_option('date_format', $tainted_fecha_vencimiento);
//$sanitized_monto = intval($tainted_monto);

//Auto Generated Values
$fecha_actual = date("Y-m-d H:i:s");
$current_user = wp_get_current_user();

//Inserting the new Assignment, using tainted variables because WP sanitizes everything
$wpdb->insert(
	'ap_tareas',
	array( 
		'date_created' => $fecha_actual, 
		'due_date' => $tainted_fecha_vencimiento,
		'user_create' =>  $current_user->user_login,
		'money_value' => $tainted_monto,
		'title' => $tainted_titulo,
		'description' => $tainted_descripcion,
		'category' => $tainted_materia,
		'user_create_id'=> $current_user->ID
	)
);

if ($wpdb->insert_id) {
	//echo "Se ha creado la asignación. Id:" . $wpdb->insert_id;
	wp_redirect('/tareas/detalle-tarea/?msg=1&id=' . $wpdb->insert_id); exit;
} elseif (!$wpdb->insert_id) {
	//echo "Error, la asignación no pudo ser creada, vuelva a intentarlo!";
	wp_redirect('/inicio/?msg=2'); exit;
}

?>