<?php
/**
 * This file manages the posting of assignments on the site.
 *
 * @author 		Oryx,Software Development
 * @version     1.0.0
 */
require_once( explode( "wp-content" , __FILE__ )[0] . "wp-load.php" );

// Make sure $_POST is set and all required fields are set too
if (empty($_POST)) {
	
	//return to Inicio and show and Error MSG: "La asignacion no pudo ser publicada, intentelo de nuevo."
	wp_redirect('/inicio/'); exit;
}

// Make sure all of the required fields are populated
if (empty($_POST['message']) || 
	empty($_POST['amount']) || 
	empty($_POST['assign_id'])) {
	
	if (!empty($_POST['assign_id'])) { //if the id is available return to that page and show message
		wp_redirect('/tareas/detalle-tarea/?id='. absint($_POST['assign_id']) .'&msg=6'); exit;
	} else { //if the id is not available go back to tareas and show message
		wp_redirect('/tareas/?msg=6'); exit;
	}
}

date_default_timezone_set("America/Santo_Domingo");

//Auto Generated Values
$fecha_actual = date("Y-m-d H:i:s");
$current_user = wp_get_current_user();

//Variables to hold the values without being sanitized
$tainted_monto=$_POST['amount'];
$tainted_message = $_POST['message'];
$tainted_assign_id = $_POST['assign_id'];

$assigns = $wpdb->get_results("SELECT * FROM ap_tareas WHERE id_assign = $tainted_assign_id");
if (count($assigns) > 0) { //We check if the result from the query is empty
	$assign = $assigns[0];
}
//If the current user is the one that created the assignment then 
//it's redirected to the Assignment Detail Page and get's an error Message
if ($assign->user_create_id == get_current_user_id()) {
	wp_redirect('/tareas/detalle-tarea/?id='. absint($_POST['assign_id']) .'&msg=8'); exit;
}

//$proposals= $wpdb->get_var( "SELECT COUNT(*) FROM ap_propuestas WHERE id_assign = $row->id_assign" );

//Here we'll check if the user has already sent a proposal for this particular assignation
$proposals_current_user = $wpdb->get_var( "SELECT COUNT(*) FROM ap_propuestas WHERE id_assign = $tainted_assign_id AND user_create_id = $current_user->ID" );
if ($proposals_current_user>0) {
	wp_redirect('/tareas/detalle-tarea/?id='. absint($_POST['assign_id']) .'&msg=9'); exit;
}

//Creating the new proposal
$wpdb->insert(
	'ap_propuestas',
	array( 
		'date_created' => $fecha_actual,
		'user_create' =>  $current_user->user_login,
		'user_create_id' =>  $current_user->ID,
		'proposal_value' => $tainted_monto,
		'message' => $tainted_message,
		'id_assign' => $tainted_assign_id
	)
);

if ($wpdb->insert_id) {
	//Notify the user that the proposal has been created successfuly
	wp_redirect('/tareas/detalle-tarea/?id='. absint($tainted_assign_id) .'&msg=7'); exit;

} elseif (!$wpdb->insert_id) {
	//Notify the user that there has been an error creating the proposal
	wp_redirect('/tareas/detalle-tarea/?id='. absint($tainted_assign_id) .'&msg=6'); exit;
}
