<?php
/**
 * This file manages the deletion of assignments on the site.
 * At this time assignments are not eliminated, it's status field 
 * is changed to 'deleted' and are no longer showed to the client.
 *
 * @author 		Oryx,Software Development
 * @version     1.0.0
 */
require_once( explode( "wp-content" , __FILE__ )[0] . "wp-load.php" );
spl_autoload_register(function ($class) {
	$filepath = realpath (dirname(__FILE__));
	include_once ( $filepath . '/../classes/' . $class . '.class.php');
});


// TODO: Make sure that the ID field is on the GET variable
// TODO: If the ID variable is not on the GET Variable then take the user to the Tareas Page and show error message
// TODO: If the ID variable is on the GET Variable but the user is not authorized to change it then go to that assignment page and show an error message.
if( empty($_GET['id']) || absint($_GET['id']) <= 0 ) {

	//Notify the user that there has been an error eliminating the assignment
	wp_redirect('/tareas/&msg=12'); exit;
}

$idAssign = absint($_GET['id']);
$currentUserID = get_current_user_id();

$currentAssign = new Assignment($idAssign);

$userPosted = $currentAssign->getUser_create_id();
$isCurrentUserCreator = ($userPosted == $currentUserID) ? true:false;

if ($isCurrentUserCreator) {

	$assignmentsManager = new AssignmentsManager();
	$assignmentsManager->deleteAssignmentByID($idAssign);

	wp_redirect('/tareas/?msg=11'); exit;
	
} else {
	wp_redirect('/tareas/&msg=12'); exit;
}
